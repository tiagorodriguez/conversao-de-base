# Algoritmo para Conversão de Base - Método Divisão Sucessivas

## Descrição


## Como utilizar o CONVERSOR DE BASE

Para usar a biblioteca adicione o `convert.h` no seu código:

```c
#include <stdio.h>
#include "convert.h"
```

Para rodar exemplo, use o seguinte comando:

```bash
gcc ./src/main.c -I ./Include/ -o main
./main
```


## Autores
Esse algoritmo foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9867357/avatar.png?width=400)  | Danilo Pietrobon Neto | DaniloNeto1 | [danilopietrobon@alunos.utfpr.edu.br](mailto:danilopietrobon@alunos.utfpr.edu.br) |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9983024/avatar.png?width=400) | Gabriel Henrique Silvestre De Lima | gabriellima.2002 | [gabriellima.2002@alunos.utfpr.edu.br](mailto:gabriellima.2002@alunos.utfpr.edu.br) |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9982821/avatar.png?width=400)  | Tiago Aureliano Lança Rodrigues | tiagorodriguez | [tiagorodriguez@alunos.utfpr.edu.br](mailto:tiagorodriguez@alunos.utfpr.edu.br) |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9867497/avatar.png?width=400)  | Eduardo Mateus Immig | [@eimmig](https://gitlab.com/) | [eimmig@alunos.utfpr.edu.br](mailto:eimmig@alunos.utfpr.edu.br)





